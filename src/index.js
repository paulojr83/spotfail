import React, {Component} from 'react';
import { Provider } from "react-redux";
import {View, Text} from 'react-native';

import 'config/ReactotronConfig';
import 'config/ReactDevtools';

import store from "./store";

const App =()=>(
    <Provider store={store}>
      <View >
        <Text>Teste</Text>
      </View>
    </Provider>
)

export default App;
