import axios from "axios";

const api = axios.create({
  baseURL: 'http://10.108.27:3000',
})

export default api;
