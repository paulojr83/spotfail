import React, { Component } from "react";

import { View, Text } from "react-native";

export default class Todo extends Component{

    static defaultProps = {
        title:'NaN'
    }

    render(){
        return (
            <View>
                <Text>{this.props.title}</Text>
            </View>
        )
    }
}